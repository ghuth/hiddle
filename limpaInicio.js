// ==UserScript==
// @name     Limpa dropdown Moodle
// @version  0.1
// @grant    none
// @include  https://moodle.ufrgs.br/*
// @author   G. Huth
// ==/UserScript==

(function() {
var listaDeA = document.getElementsByTagName("a");
var padraoDiciplinaUFRGS = /^[A-Z]{3}[0-9]{3}/;
var quantDeDiciplinas    = 0;
var subtraiDisciplina    = 0;
var contadorFor          = 0;
var disciplinasAtuais    = 6;

for (contadorFor = listaDeA.length - 1; contadorFor > 0; contadorFor--){
  if (padraoDiciplinaUFRGS.test(listaDeA[contadorFor].getAttribute("title")) &&
      !(listaDeA[contadorFor].getAttribute('itemprop'))){
    quantDeDiciplinas++;
  }
  else if (quantDeDiciplinas + subtraiDisciplina > disciplinasAtuais){
    listaDeA[quantDeDiciplinas+contadorFor].remove();
    subtraiDisciplina--;
  }
}

})();
