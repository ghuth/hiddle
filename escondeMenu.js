// ==UserScript==
// @name     Esconde disciplinas Moodle
// @version  0.1
// @grant    none
// @include  https://moodle.ufrgs.br/*
// @author   G. Huth
// ==/UserScript==

(function() {
    var curso_home = document.getElementsByClassName(' span4 panel panel-default coursebox hover');
    for (let i = curso_home.length-1; i >= 10; i--) {
        curso_home[i].remove();
    }
})();
